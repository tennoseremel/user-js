// ==UserScript==
// @name           Point customizer
// @name:ru        Point кастомизированный
// @version        1.1
// @namespace      tenno-seremel
// @author         Tenno Seremel
// @description    Changes logo link to //point.im/all.
// @description:ru Меняет ссылку в логотипе на //point.im/all.
// @include        http://point.im/*
// @include        https://point.im/*
// @include        http://*.point.im/*
// @include        https://*.point.im/*
// @grant          none
// @run-at         document-end
// ==/UserScript==
'use strict';

function corner_link_to_all()
{
	let el = document.getElementById('logo');
	if (el) {
		el.setAttribute('href', '//point.im/all');
	}
}

corner_link_to_all();
