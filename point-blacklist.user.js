// ==UserScript==
// @name           Point blacklist
// @name:ru        Point - чёрные списки
// @version        1.8.2
// @namespace      tenno-seremel
// @author         Tenno Seremel
// @description    Configuration inside.
// @description:ru Конфигурация внутри.
// @include        https://point.im/all*
// @include        https://*.point.im/*
// @exclude        https://*.point.im/messages*
// @exclude        https://*.point.im/bookmarks*
// @exclude        https://*.point.im/comments*
// @exclude        https://*.point.im/profile*
// @exclude        https://*.point.im/subscriptions*
// @exclude        https://*.point.im/subscribers*
// @exclude        https://*.point.im/tags*
// @include        http://point.im/all*
// @include        http://*.point.im/*
// @exclude        http://*.point.im/messages*
// @exclude        http://*.point.im/bookmarks*
// @exclude        http://*.point.im/comments*
// @exclude        http://*.point.im/profile*
// @exclude        http://*.point.im/subscriptions*
// @exclude        http://*.point.im/subscribers*
// @exclude        http://*.point.im/tags*
// @grant          none
// @run-at         document-end
// ==/UserScript==

'use strict';
/*
notification_block_id:
	id элемента, в который будет выводиться информация о том,
	что сообщения были удалены (может быть null, если эта информация не нужна)
	на текущий момент это, например: content (блок с сообщениями), left-menu (сайдбар)
notification_is_at_top:
	Добавить текст уведомления до уже имеющегося текста в блоке (true) или после (false)
notification_wrapper_style
	CSS стиль для блока с текстом нотификации
tag_black_list:
	массив запрещённых тегов (строки должны быть в нижнем регистре)
user_black_list:
	массив запрещённых пользователей (строки должны быть в нижнем регистре)
user_tag_black_list
	хэш запрещённых тегов по пользователям (строки должны быть в нижнем регистре)
	Например.: user_tag_black_list: { 'ololo': ['lololo'], 'user2': ['тег1', 'тег2'] }
	Значением элемента хэша должен быть массив, как в примере, даже если перечислен всего 1 тег.
block_posts_without_tags:
	установить в true для скрытия постов без тегов
block_comments:
	установить в true для скрытия в том числе и комментариев пользователей в user_black_list
*/
var CONFIG = {
	notification_block_id: 'left-menu',
	notification_is_at_top: true,
	notification_wrapper_style: 'margin: 0 0 1em 0;',
	block_posts_without_tags: true,
	block_comments: false,
	tag_black_list: [
		// 'arduino'
	],
	user_black_list: [
		// 'irsi'
	],
	user_tag_black_list: {
		// 'nokitakaze': ['overlord', 'shimoneta']
	}
};
// posts
var RULE_EACH_POST = '#content div.post';
var RULE_POST_AUTHOR = '.post-author'; // inside post
var RULE_POST_TAGS = '.post-tag'; // inside post
// comments
var RULE_EACH_COMMENT = '#comments div.post';
var RULE_COMMENT_AUTHOR = '.post-author'; // inside comment
var RULE_COMMENT_HIDE_WHAT = '.post-content > .text, .post-content > .files'; // inside comment
var RULE_COMMENTS_SHOW = RULE_EACH_COMMENT + ' .post-content > .text, ' + RULE_EACH_COMMENT + ' .post-content > .files';
function Posts()
{
	this._posts_hidden = 0;
	this._dotElement = document.createElement('div');
	this._dotElement.setAttribute('style', 'width: 8px;height: 8px;background: #B8120F;border-radius: 4px;position: absolute;top: 4px;right: 0;');
}
Posts.prototype.hideOne = function(el) {
	el.style.display = 'none';
	this._posts_hidden += 1;
};
Posts.prototype.hideList = function(els) {
	for (var item of els) {
		item.style.display = 'none';
		if (!item.classList.contains('files')) {
			++MESSAGES_REMOVED;
		}
	}
};
Posts.prototype.getTotalRemovedCount = function() {
	return this._posts_hidden;
};
Posts.prototype.isHidden = function(el) {
	return (el.style.display === 'none');
};
Posts.prototype.showOne = function(el) {
	if (this.isHidden(el)) {
		el.style.display = '';
		if (!el.classList.contains('files')) {
			el.style.position = 'relative';
			el.appendChild(this._dotElement.cloneNode(false));
		}
	}
};
Posts.prototype.showAll = function() {
	var rule = document.getElementById('comments') ? RULE_COMMENTS_SHOW : RULE_EACH_POST;
	var els = document.querySelectorAll(rule);
	for (var post of els) {
		this.showOne(post);
	}
};
Posts.prototype.applyBlacklistToComments = function() {
	var comments = document.querySelectorAll(RULE_EACH_COMMENT);
	for (var comment of comments) {
		var author = comment.querySelector(RULE_COMMENT_AUTHOR);
		if (author) {
			author = author.textContent.trim().toLowerCase();
			if (CONFIG.user_black_list.indexOf(author) !== -1) {
				this.hideList(comment.querySelectorAll(RULE_COMMENT_HIDE_WHAT));
			}
		}
	}
};
Posts.prototype.applyBlacklistToPosts = function() {
	var messages = document.querySelectorAll(RULE_EACH_POST);
	for (var message of messages) {
		// tags
		var tag_elements = message.querySelectorAll(RULE_POST_TAGS);
		if (!tag_elements.length && CONFIG.block_posts_without_tags) {
			// no tags
			this.hideOne(message);
		} else {
			var not_found = true;
			// author or null
			var author = message.querySelector(RULE_POST_AUTHOR);
			if (author) {
				author = author.textContent.trim().toLowerCase();
				if (CONFIG.user_black_list.indexOf(author) !== -1) {
					// blacklisted author
					this.hideOne(message);
					not_found = false;
				}
			}
			if (not_found) {
				var message_user_tag_black_list = CONFIG.user_tag_black_list[author];
				for (var tag_element of tag_elements) {
					var message_tag = tag_element.textContent.trim().toLowerCase();
					if (
						(CONFIG.tag_black_list.indexOf(message_tag) !== -1)
						|| (
							message_user_tag_black_list
							&& (message_user_tag_black_list.indexOf(message_tag) !== -1)
						)
					) {
						// blacklisted tag, either global or some user's
						this.hideOne(message);
						break;
					}
				}
			}
		}
	}
};
function Notifier(container_node)
{
	this._container_node = container_node;
	this._output_el = document.createElement('div');
	this._output_el.setAttribute('style', CONFIG.notification_wrapper_style);
	this._button = document.createElement('button');
	this._button.setAttribute('type', 'button');
	this._button.setAttribute('title', 'Показать скрытые.');
	this._button.setAttribute('style', 'padding: .2em 1ex;');
	this._output_el.appendChild(this._button);
}
Notifier.prototype.show = function(message, button_handler) {
	this._button.textContent = message;
	this._button.addEventListener('click', button_handler, false);
	if (CONFIG.notification_is_at_top) {
		this._container_node.insertBefore(this._output_el, this._container_node.firstChild);
	} else {
		this._container_node.appendChild(this._output_el);
	}
};
Notifier.prototype.hide = function() {
	this._output_el.parentNode.removeChild(this._output_el);
};
var posts = new Posts();
if (document.getElementById('comments')) {
	// inside post with comments
	if (CONFIG.block_comments) {
		posts.applyBlacklistToComments();
	}
} else {
	// /all, /blog, maybe something else which shouldn't find anything (hopefully)
	posts.applyBlacklistToPosts();
}
var total_removed = posts.getTotalRemovedCount();
if (total_removed) {
	var container = document.getElementById(CONFIG.notification_block_id);
	if (container) {
		var n = new Notifier(container);
		n.show(
			'Скрыто: ' + total_removed + '.',
			(function(){
				this.posts.showAll();
				this.notifier.hide();
			}).bind({ posts: posts, notifier: n })
		);
	}
}
